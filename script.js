function GetNumber() {
	number = document.getElementById('number').value;
	}
	
function PrintResult() {
	document.getElementById('resultat').innerHTML = resultat;
	document.getElementById('resultat1').innerHTML = resultat1;
	document.getElementById('resultat2').innerHTML = resultat2;	
	}
	
function Translation_to2SS() {
	GetNumber();
		
		if(isNaN(number)) {
			result = 'Неккоректный ввод!';
			document.getElementById('result').innerHTML = result;
		}
	
		else {
				document.getElementById('koment').innerHTML = 'Число в 8 - ой с/с:';
				resultat = parseInt(number, 2).toString(8);
			
				document.getElementById('koment1').innerHTML = 'Число в 10 - ой с/с:';
				resultat1 = parseInt(number, 2).toString(10);
			
				document.getElementById('koment2').innerHTML = 'Число в 16 - ой с/с:';
				resultat2 = parseInt(number, 2).toString(16);
				
	PrintResult();
		}
	}

function Translation_to8SS() {
	GetNumber();
		
		if(isNaN(number)) {
			result = 'Неккоректный ввод!';
			document.getElementById('result').innerHTML = result;
			}
		else {
			document.getElementById('koment').innerHTML = 'Число в 2 - ой с/с:';
			resultat = parseInt(number, 8).toString(2);
			
			document.getElementById('koment1').innerHTML = 'Число в 10 - ой с/с:';
			resultat1 = parseInt(number, 8).toString(10);
			
			document.getElementById('koment2').innerHTML = 'Число в 16 - ой с/с:';
			resultat2 = parseInt(number, 8).toString(16);
		
	PrintResult();
			}
	}

function Translation_to10SS() {
	GetNumber();	
		
		if(isNaN(number)) {
			result = 'Неккоректный ввод!';
			document.getElementById('result').innerHTML = result;
			}
		else {
			document.getElementById('koment').innerHTML = 'Число в 2 - ой с/с:';
			resultat = parseInt(number, 10).toString(2);
			
			document.getElementById('koment1').innerHTML = 'Число в 8 - ой с/с:';
			resultat1 = parseInt(number, 10).toString(8);
			
			document.getElementById('koment2').innerHTML = 'Число в 16 - ой с/с:';
			resultat2 = parseInt(number, 10).toString(16);
			
	PrintResult();
		}
	}

function Translation_to16SS() {
	GetNumber();
		
		if(isNaN(number)) {
			result = 'Неккоректный ввод!';
			document.getElementById('result').innerHTML = result;
		}
		else {
			document.getElementById('koment').innerHTML = 'Число в 2 - ой с/с:';
			resultat = parseInt(number, 16).toString(2);
		
			document.getElementById('koment1').innerHTML = 'Число в 8 - ой с/с:';
			resultat1 = parseInt(number, 16).toString(8);
		
			document.getElementById('koment2').innerHTML = 'Число в 10 - ой с/с:';
			resultat2 = parseInt(number, 16).toString(10);
			
	PrintResult();
		}
	}